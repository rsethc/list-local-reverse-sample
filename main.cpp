#include <string>
#include <iostream>
using namespace std;

template <typename itemtype>
struct reversable_doublell
{
    struct node
    {
        itemtype item;
        node* next;
        node* prev;
    };
    node* first = NULL;
    node* last = NULL;
    void add (itemtype item)
    {
        node* created = new node;
        created->item = item;
        created->next = NULL;
        created->prev = last;
        if (first) last->next = created;
        else first = created;
        last = created;
    }
    void reverse (node* local_first, node* local_last)
    {
        node* was_before_first = local_first->prev;
        node* was_after_last = local_last->next;
        if (local_first->prev) local_first->prev->next = local_last;
        else first = local_last;
        if (local_last->next) local_last->next->prev = local_first;
        else last = local_first;
        node* here = local_first;
        while (true)
        {
            node* was_next = here->next;
            here->next = here->prev;
            here->prev = was_next;
            if (here == local_last) break;
            here = was_next;
        }
        local_first->next = was_after_last;
        local_last->prev = was_before_first;
    }
};

reversable_doublell<string> list;
void print ()
{
    cout << "start" << endl;
    reversable_doublell<string>::node* here = list.first;
    while (here)
    {
        cout << "\t" << here->item << endl;
        here = here->next;
        static int i = 30;
        if (!--i) break;
    }
    cout << "end" << endl;
}

int main ()
{
    list.add("a");
    list.add("b");
    list.add("c");
    list.add("d");
    list.add("e");
    list.add("f");
    print();

    cout << "second through fifth" << endl;
    list.reverse(list.first->next,list.last->prev);
    print();

    cout << "fourth and fifth" << endl;
    list.reverse(list.first->next->next->next,list.last->prev);
    print();

    cout << "one element" << endl;
    list.reverse(list.first->next->next->next,list.last->prev->prev);
    print();

    cout << "edge case, first" << endl;
    list.reverse(list.first,list.first);
    print();

    cout << "edge case, last" << endl;
    list.reverse(list.last,list.last);
    print();

    cout << "all" << endl;
    list.reverse(list.first,list.last);
    print();

    return 0;
}
